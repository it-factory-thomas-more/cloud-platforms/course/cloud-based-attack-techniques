# Cloud-based Attack Techniques

## Objectives

- Understand the basics of cloud-based attack techniques.
- Learn how to use cloud-based attack techniques to launch attacks.

## Introduction

Cloud-based attack techniques are a set of methods that attackers use to exploit cloud services and infrastructure. These techniques leverage the features and capabilities of cloud platforms to launch attacks against cloud-based applications and services. In this lesson, we will explore some common cloud-based attack techniques and learn how attackers can use them to compromise cloud environments.

## MITRE ATT&CK Framework

![](img/MITRE.png)

The [MITRE ATT&CK Framework](https://attack.mitre.org/) is a knowledge base of adversary tactics and techniques based on real-world observations. It provides a comprehensive view of the tactics and techniques that attackers use to compromise systems and networks. The framework is widely used in cybersecurity to categorize and analyze cyber threats.

MITRE ATT&CK uses tactic categories to organize the techniques. The tactics are divided into several categories. Some of the common tactics include:


| Tactic            | Description                                                                 |
|-------------------|-----------------------------------------------------------------------------|
| Initial Access    | Techniques used by attackers to gain initial access to a target system.     |
| Execution         | Techniques used by attackers to execute malicious code on a target system. |
| Persistence       | Techniques used by attackers to maintain access to a target system, for example, across system restarts.         |
| Privilege Escalation | Techniques used by attackers to gain higher privileges on a target system. |
| Defense Evasion   | Techniques used by attackers to avoid detection by security tools.          |
| Credential Access | Techniques used by attackers to steal credentials from a target system.     |
| Discovery         | Techniques used by attackers to gather information about a target system in order to gain more knowledge about the environment.  |
| Lateral Movement  | Techniques used by attackers to move from one remote system to another.   |
| Collection        | Techniques used by attackers to gather data that is useful to the adversary from a target system.           |
| Exfiltration      | Techniques used by attackers to steal data from the network, preferably while avoiding detection.      |
| Command and Control | Techniques used by attackers to communicate with a compromised system.     |
| Impact            | Techniques used by attackers to cause damage to a target system.            |


You can learn more about the MITRE ATT&CK Framework by viewing this YouTube video:

[![MITRE ATT&CK Framework](https://img.youtube.com/vi/Yxv1suJYMI8/0.jpg)](https://www.youtube.com/embed/Yxv1suJYMI8?si=BTE8adtNoMlpgcWP)


## MITRE ATT&CK Cloud Matrix

The [MITRE ATT&CK Cloud Matrix](https://attack.mitre.org/matrices/enterprise/cloud/#) applies the ATT&CK framework to cloud environments. It can be applied to all major cloud service providers, including Amazon Web Services (AWS), Microsoft Azure, and Google Cloud Platform (GCP).

![Mitre ATT&CK Cloud Matrix](img/2024-04-24-13-40-05.png){width=50%}

## PwnedLabs

[PwnedLabs](https://pwnedlabs.io/) is a platform that provides hands-on labs and training for cybersecurity professionals. It offers a wide range of labs that cover various topics, including cloud security, penetration testing, and incident response. We will be using PwnedLabs to practice cloud-based attack techniques.

> **✅ TASK:** Create an account on [PwnedLabs](https://pwnedlabs.io/) and explore the available labs.


## Common Cloud-Based Attack Techniques

### [Initial Access Tactic (TA0001)](https://attack.mitre.org/tactics/TA0001/)

![Comic about Phishing Attacks](img/phishing_comic.jpg){width=50%}

The Initial Access tactic includes techniques that attackers use to gain a first access to a target cloud environment. This involves finding some way to get into the target environment, such as exploiting a vulnerability or using stolen credentials. Some of the common techniques under this tactic include:

- [**Phishing (T1566)**](https://attack.mitre.org/techniques/T1566/): Attackers may use phishing emails to trick users into revealing their credentials or clicking on malicious links.

- [**Valid Accounts (T1078)**](https://attack.mitre.org/techniques/T1078/): Attackers may use valid accounts to gain access to a target cloud environment. This can be done by stealing credentials or using compromised accounts.

- [**Exploit Public-Facing Application (T1190)**](https://attack.mitre.org/techniques/T1190/): Attackers may exploit vulnerabilities in public-facing applications to gain access to a target cloud environment.

> **📊 EXAMPLE:**
> The [Siloscape malware](https://unit42.paloaltonetworks.com/siloscape/) was discovered in 2021 and was designed to target Windows containers in the cloud. It exploited a vulnerability in the Windows container runtime to escape the container and gain access to the underlying Kubernetes cluster. This allowed the attackers to gain access to the cloud environment and execute malicious code.
> ![Execution flow of Siloscape.](img/Siloscape%20Windows%20Malware.jpeg)

> **✅ TASK:** Complete the [Azure Blob Container to Initial Access Lab](https://pwnedlabs.io/labs/azure-blob-container-to-initial-access). It shows you how a few simple misconfigurations of the blob storage account hosting a static website can lead to a full compromise of the Azure environment.


### [Credential Access Tactic (TA0006)](https://attack.mitre.org/tactics/TA0006/)

The Credential Access tactic includes ways in which attackers can obtain credentials to access cloud environments. This can involve stealing credentials, cracking passwords, or using other methods to gain access to user accounts. A lot of the known tools an techniques used in on-premises environments can be used in cloud environments as well. Some of the common techniques under this tactic include:

- [**Brute Force (T1110)**](https://attack.mitre.org/techniques/T1110/): Attackers may use brute force attacks to guess passwords and gain access to user accounts using tools like Hydra, Medusa, or Burp Suite.

- [**Network Sniffing (T1040)**](https://attack.mitre.org/techniques/T1040/): Attackers may use network sniffing tools to capture network traffic and steal credentials.

- [**Multi-factor Request Generation (T1621)**](https://attack.mitre.org/techniques/T1621/): Adversaries who are in the possession of [Valid Accounts](https://attack.mitre.org/techniques/T1078/) may be unable to log in to the cloud environment due to multi-factor authentication. In such cases, attacker can trigger MFA notifications to the victim's device and capture the MFA code. Adversaries can also continuously generate MFA requests to the victim's device, bombarding them with notifications. This potentially results in the victim accepting the MFA request in response to "MFA fatigue".

> **📊 EXAMPLE:**
> The [Google Cloud Threat Horizon Report 2024](https://services.google.com/fh/files/misc/threat_horizons_report_h12024.pdf) states that credentials issues within cloud environments are the predominant attack vector. More than 50% of the compromised cloud instances were due to weak or default credentials.
> ![Pie chart 2023 Cloud Compromises: Initial Access](img/2024-05-06-09-23-37.png){width=50%}

> **✅ TASK:** Within the [Pillage Exposed RDS Instances Lab](https://pwnedlabs.io/labs/pillage-exposed-rds-instances), you will learn how to use traditional network scanning and password cracking techniques to gain access to an AWS RDS instance. Don't forget to read the *Defense* section to learn how to protect your cloud environment from such attacks.
> **TIP**: If the provided `nmap --script=mysql-brute` command does not work, you can perform the same attack using `hydra -C mysql-creds.txt`.


### [Discovery Tactic (TA0007)](https://attack.mitre.org/tactics/TA0007/)

The Discovery tactic includes several techniques that could be used by attackers to gather information about the target cloud environment and allow an attacker to acquire things like account names, group information, and cloud service configurations. Some of the common techniques under this tactic include:

- [**Cloud Service Discovery (T1526)**](https://attack.mitre.org/techniques/T1526/): Attackers may attempt to enumerate the cloud services that are available in the target environment. 
- [**Cloud Account Discovery (T1087.004)**](https://attack.mitre.org/techniques/T1087/004/): Attackers may attempt to create listings of cloud accounts. Command-line tools like `aws iam list-users` or `az ad user list` can be used to list cloud accounts.
- [**Cloud Permission Groups Discovery (T1069.003)**](https://attack.mitre.org/techniques/T1069/003/): Attackers may attempt to enumerate cloud groups and their associated permissions. The can give an attacker insights into the possible attack vectors that can be exploited.
- [**Cloud Storage Object Discovery (T1619)**](https://attack.mitre.org/techniques/T1619/): Attackers may attempt to enumerate cloud storage objects to identify sensitive data that can be exfiltrated. Tools like `aws s3 ls` or `az storage blob list` can be used to list storage objects.

> **📊 EXAMPLE:**
> Specialized tools like [Pacu](https://github.com/RhinoSecurityLabs/pacu) or [AADInternals](https://aadinternals.com/) can be used to automate the discovery process.
> The screenshot below shows `pacu` being used to discover cloud services in an unauthenticated way, using only the account id.
> ![Screenshot of AWS Services being discovered with Pacu](img/AWS%20Services.png){width=50%}

> **✅ TASK:** Complete the [Pwnedlabs Intro to AWS IAM Enumeration Lab](https://pwnedlabs.io/labs/intro-to-aws-iam-enumeration) to get acquainted with the techniques used to discover AWS IAM roles and permissions.

### [Collection Tactic (TA0009)](https://attack.mitre.org/tactics/TA0009/)

In a lot of cases, attackers want to gather data that is useful to them. This can include sensitive information, credentials, or other data that can be used to further compromise the target environment. 

One of the most common examples is the collection of [Data from Cloud Storage](https://attack.mitre.org/techniques/T1530/). Attackers may attempt to collect and exfiltrate data from cloud storage services like Amazon S3, Azure Blob Storage, or Google Cloud Storage.

> **📊 EXAMPLE:**
> In 2022, [a misconfigured Amazon S3 bucket containing 3TB of airport data was discovered by security researchers.](https://www.darkreading.com/application-security/cloud-misconfig-exposes-3tb-sensitive-airport-data-amazon-s3-bucket) The bucket was configured as public, without any authentication requirement for access. The leaked data included empoyee personal information and other sensitive data affecting at least four airports in Colombia and Peru. An additional list of other S3-based leaks can be found [here.](https://github.com/nagwww/s3-leaks)

> **✅ TASK:** Complete the [SSRF to Pwned Lab](https://pwnedlabs.io/labs/ssrf-to-pwned) to learn how to exploit Server-Side Request Forgery (SSRF) vulnerabilities. This leads to getting access to the EC2 metadata service in order to retrieve credentials, which allow access to an S3 bucket that contains sensitive data.
